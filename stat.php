<?php 

$dirPath = __DIR__ . '/teste1/teste2/teste3/teste4.txt';

/**
 * MODOS DE ACESSO NA MANIPULAÇÃO DE ARQUIVOS
 * 
 * W - Coloca o ponteiro no início do arquivo e diminui o tamanho do arquivo para zero;
 * 
 * R - Coloca o ponteiro no início do arquivo e não exclói o conteúdo
 * 
 * A - Coloca o ponteiro no final do arquivo e soma o conteúdo
 */

$file = fopen("./teste1/teste2/teste3/teste4.txt","r+");

$retorno = fgets($file);

$retorno = fgetc($file);

$ponteiroPositionAtual = ftell($file);

print_r("Posição atual ponteiro: " . PHP_EOL);
print_r($ponteiroPositionAtual . PHP_EOL);

$retorno = fgetc($file);

$ponteiroPosition = fseek($file,1);

print_r("Posição atual ponteiro após fseek(): " . PHP_EOL);
print_r($ponteiroPositionAtual . PHP_EOL);

$retorno = fgetc($file);

$retorno = fpassthru($file);

fclose($file);

print_r(PHP_EOL);



