<?php

function xrange($start, $limit, $step = 1) {
    if ($start < $limit) {
        if ($step <= 0) {
            throw new LogicException('Step must be +ve');
        }

        for ($i = $start; $i <= $limit; $i += $step) {
            yield $i;
        }
    } else {
        if ($step >= 0) {
            throw new LogicException('Step must be -ve');
        }

        for ($i = $start; $i >= $limit; $i += $step) {
            yield $i;
        }
    }
}

/*
 * Note that both range() and xrange() result in the same
 * output below.
 */

echo 'Single digit odd numbers from range():  ';
foreach (range(1, 9, 2) as $number) {
    echo "$number ";
}
echo "\n";

echo "Debugging xrange(): \n";
$obGenerator = xrange(1,9,2);

while(!$obGenerator->valid()) {

    echo "\nPróximo valor: \n";
    $obGenerator->next();
    $current = $obGenerator->current();
    $key     = $obGenerator->key();

    echo "Chave: $key\n";
    echo "Valor: $current\n";

}


?>
