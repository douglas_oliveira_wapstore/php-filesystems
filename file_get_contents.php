<?php 

$dirPath = __DIR__ . '/teste.txt';

/**
 * MODOS DE ACESSO NA MANIPULAÇÃO DE ARQUIVOS
 * 
 * W - Coloca o ponteiro no início do arquivo e diminui o tamanho do arquivo para zero;
 * 
 * R - Coloca o ponteiro no início do arquivo e não exclói o conteúdo
 * 
 * A - Coloca o ponteiro no final do arquivo e soma o conteúdo
 */

$file = fopen("./teste1/teste2/teste3/teste4.txt","r");

$string = file_get_contents("./teste1/teste2/teste3/teste4.txt");

var_dump($string);

print_r(PHP_EOL);



